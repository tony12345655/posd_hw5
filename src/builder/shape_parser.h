#pragma once

#include "./scanner.h"
#include "./shape_builder.h"
#include "../two_dimensional_vector.h"
#include <string>
#include <vector>

class ShapeParser
{
private:
    Scanner *_scanner;
    ShapeBuilder *_builder;

public:
    ShapeParser(std::string input) {
        _scanner = new Scanner(input);
        _builder = new ShapeBuilder();
    }

    ~ShapeParser() {
        delete this->_scanner;
        delete this->_builder;
    }

    void parse() {
        while (!this->_scanner->isDone())
        {
            std::string token = this->_scanner->next();
            if (token == "Circle"){
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                double x1 = this->_scanner->nextDouble();
                this->_scanner->next();
                double y1 = this->_scanner->nextDouble();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                double x2 = this->_scanner->nextDouble();
                this->_scanner->next();
                double y2 = this->_scanner->nextDouble();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                Point p1(x1, y1);
                Point p2(x2, y2);
                this->_builder->buildCircle(p1, p2);
            }
            else if (token == "Triangle"){
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                double x1 = this->_scanner->nextDouble();
                this->_scanner->next();
                double y1 = this->_scanner->nextDouble();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                double x2 = this->_scanner->nextDouble();
                this->_scanner->next();
                double y2 = this->_scanner->nextDouble();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                double x3 = this->_scanner->nextDouble();
                this->_scanner->next();
                double y3 = this->_scanner->nextDouble();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                double x4 = this->_scanner->nextDouble();
                this->_scanner->next();
                double y4 = this->_scanner->nextDouble();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                Point p1(x1, y1);
                Point p2(x2, y2);
                Point p3(x3, y3);
                Point p4(x4, y4);
                if (p1 == p2)
                    this->_builder->buildTriangle(p1, p3, p4);
                else if (p1 == p3)
                    this->_builder->buildTriangle(p1, p2, p4);
                else if (p1 == p4)
                    this->_builder->buildTriangle(p1, p2, p3);
                else if (p2 == p3)
                    this->_builder->buildTriangle(p2, p1, p4);
                else if (p2 == p4)
                    this->_builder->buildTriangle(p2, p1, p3);
                else if (p3 == p4)
                    this->_builder->buildTriangle(p3, p1, p2);
            }
            else if (token == "Rectangle"){
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                double x1 = this->_scanner->nextDouble();
                this->_scanner->next();
                double y1 = this->_scanner->nextDouble();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                double x2 = this->_scanner->nextDouble();
                this->_scanner->next();
                double y2 = this->_scanner->nextDouble();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                double x3 = this->_scanner->nextDouble();
                this->_scanner->next();
                double y3 = this->_scanner->nextDouble();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                double x4 = this->_scanner->nextDouble();
                this->_scanner->next();
                double y4 = this->_scanner->nextDouble();
                this->_scanner->next();
                this->_scanner->next();
                this->_scanner->next();
                Point p1(x1, y1);
                Point p2(x2, y2);
                Point p3(x3, y3);
                Point p4(x4, y4);
                if (p1 == p2)
                    this->_builder->buildRectangle(p1, p3, p4);
                else if (p1 == p3)
                    this->_builder->buildRectangle(p1, p2, p4);
                else if (p1 == p4)
                    this->_builder->buildRectangle(p1, p2, p3);
                else if (p2 == p3)
                    this->_builder->buildRectangle(p2, p1, p4);
                else if (p2 == p4)
                    this->_builder->buildRectangle(p2, p1, p3);
                else if (p3 == p4)
                    this->_builder->buildRectangle(p3, p1, p2);
            }
            else if (token == "CompoundShape"){
                this->_builder->buildCompoundShape();
                this->_scanner->next();
            }
            else if (token == ")")
                this->_builder->buildCompoundEnd();

        }
        
    }

    std::vector<Shape*> getResult() { return this->_builder->getResult(); }
};
