#pragma once

#include "shape_visitor.h"
#include "../shape.h"
#include "../circle.h"
#include "../triangle.h"
#include "../rectangle.h"
#include "../compound_shape.h"
#include "../bounding_box.h"

#include <vector>

class CollisionDetector : public ShapeVisitor {
    private:
        std::vector<Shape *> _collideResult;
        BoundingBox *_targetBoundingBox;
    public:
        CollisionDetector(Shape* shape) {
            this->_targetBoundingBox = new BoundingBox(shape->getPoints());
        }
        
        ~CollisionDetector() {
            delete this->_targetBoundingBox;
        }

        void visitCircle(Circle* circle) override {
            BoundingBox* circle_bounding = new BoundingBox(circle->getPoints());
            if (this->_targetBoundingBox->collide(circle_bounding))
                this->_collideResult.push_back(circle);
            delete circle_bounding;
        }

        void visitTriangle(Triangle* triangle) override {
            BoundingBox* triangle_bounding = new BoundingBox(triangle->getPoints());
            if (this->_targetBoundingBox->collide(triangle_bounding))
                this->_collideResult.push_back(triangle);
            delete triangle_bounding;
        }

        void visitRectangle(Rectangle* rectangle) override {
            BoundingBox* rectangle_bounding = new BoundingBox(rectangle->getPoints());
            if (this->_targetBoundingBox->collide(rectangle_bounding))
                this->_collideResult.push_back(rectangle);
            delete rectangle_bounding;
        }

        void visitCompoundShape(CompoundShape* compound_shape) override {
            BoundingBox* compound_shape_bounding = new BoundingBox(compound_shape->getPoints());
            if (this->_targetBoundingBox->collide(compound_shape_bounding)){
                Iterator* it = compound_shape->createIterator(IteratorFactory::getInstance("List"));
                for(it->first(); !it->isDone(); it->next()){
                    it->currentItem()->accept(this);
                    std::vector<Shape *> scores = this->collidedShapes();
                }
                delete it;
            }      
            delete compound_shape_bounding;
        }

        std::vector<Shape *> collidedShapes() const {
            return this->_collideResult;
        }
};