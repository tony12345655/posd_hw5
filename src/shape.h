#pragma once

#include "point.h"
#include "./iterator/iterator.h"
#include "./iterator/factory/iterator_factory.h"
#include "./visitor/shape_visitor.h"

#include <string>
#include <set>

class Shape
{
public:
    virtual ~Shape() {}
    virtual double area() const = 0;
    virtual double perimeter() const = 0;
    virtual std::string info() const = 0;
    virtual Iterator* createIterator(IteratorFactory * factory) = 0;
    virtual std::set<Point> getPoints() = 0;
    virtual void accept(ShapeVisitor* visitor) = 0;
    virtual void addShape(Shape *shape) = 0;
    virtual void deleteShape(Shape *shape) = 0;

};
