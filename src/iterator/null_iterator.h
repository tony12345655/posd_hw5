#pragma once

#include "iterator.h"

class NullIterator : public Iterator
{
public:
    void first() override {
        throw "NullIterator can't use first.";
    }

    Shape* currentItem() const override {
        throw "NullIterator can't use currentItem.";
    }

    void next() override {
        throw "NullIterator can't use next.";
    }

    bool isDone() const override { return true; }
};