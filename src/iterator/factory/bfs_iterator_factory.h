#pragma once

#include "./iterator_factory.h"
#include "../iterator"
#include "../../shape.h"

class BFSIteratorFactory : public IteratorFactory {
    private:
        BFSIteratorFactory();
        static BFSIteratorFactory BFSInstance;

    public:
        Iterator* createIterator() override;
        Iterator* createIterator(std::list<Shape *>::const_iterator begin, std::list<Shape *>::const_iterator end) override;
};