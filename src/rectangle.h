#pragma once

#include "two_dimensional_vector.h"
#include "shape.h"
#include "iterator/factory/iterator_factory.h"
#include "visitor/shape_visitor.h"

#include <string>
#include <sstream>
#include <set>

class Rectangle : public Shape
{
private:
    TwoDimensionalVector _lengthVec;
    TwoDimensionalVector _widthVec;
    Point* _four_point;
    
    Point* findFourthVertex(TwoDimensionalVector lengthVec, TwoDimensionalVector widthVec) {
        double number_four_x, number_four_y;
         if (lengthVec.a() == widthVec.a()){
            number_four_x = lengthVec.b().x() + widthVec.b().x() - lengthVec.a().x();
            number_four_y = lengthVec.b().y() + widthVec.b().y() - lengthVec.a().y();
            return new Point(number_four_x, number_four_y);
        }
        else if (lengthVec.a() == widthVec.b()){
            number_four_x = lengthVec.b().x() + widthVec.a().x() - lengthVec.a().x();
            number_four_y = lengthVec.b().y() + widthVec.a().y() - lengthVec.a().y();
            return new Point(number_four_x, number_four_y);
        }
        else if (lengthVec.b() == widthVec.a()){
            number_four_x = lengthVec.a().x() + widthVec.b().x() - lengthVec.b().x();
            number_four_y = lengthVec.a().y() + widthVec.b().y() - lengthVec.b().y();
            return new Point(number_four_x, number_four_y);
        }
        else if (lengthVec.b() == widthVec.b()){
            number_four_x = lengthVec.a().x() + widthVec.a().x() - lengthVec.b().x();
            number_four_y = lengthVec.a().y() + widthVec.a().y() - lengthVec.b().y();
            return new Point(number_four_x, number_four_y);
        }
        throw "error";
    }

public:
    Rectangle(TwoDimensionalVector lengthVec, TwoDimensionalVector widthVec): _lengthVec(lengthVec), _widthVec(widthVec) {
        bool VectorLegal = this->_lengthVec.a() == this->_widthVec.a() || this->_lengthVec.a() == this->_widthVec.b() || this->_lengthVec.b() == this->_widthVec.a() || this->_lengthVec.b() == this->_widthVec.b();
        if (VectorLegal == false || this->_lengthVec.dot(_widthVec) != 0)
            throw "error";
        else
            this->_four_point = this->findFourthVertex(lengthVec, widthVec);
    }
    ~Rectangle() {
        delete this->_four_point;
    }

    double length() const { return _lengthVec.length(); }

    double width() const { return _widthVec.length(); }

    double area() const override {
        return this->length() * this->width();
    }

    double perimeter() const override {
        return 2 * (this->length() + this->width());
    }

    std::string info() const override {
        std::stringstream ss;
        ss << std::fixed << std::setprecision(2) << "Rectangle (" << this->_lengthVec.info() << ", " << this->_widthVec.info() << ")";
        std::string out = ss.str();
        
        return out; 
    }

    Iterator *createIterator(IteratorFactory *factory) override {
        return factory->createIterator();
    }

    void addShape(Shape* shape) override {
        return throw "Rectangle can't addShape.";
    }

    void deleteShape(Shape* shape) override {
        return throw "Rectangle can't deleteShape.";
    }

    std::set<Point> getPoints() {
        std::set<Point> vertex;
        vertex.insert(*this->_four_point);
        if (this->_lengthVec.a() == this->_widthVec.a()){
            vertex.insert(this->_lengthVec.a());
            vertex.insert(this->_lengthVec.b());
            vertex.insert(this->_widthVec.b());
        }
        else if (this->_lengthVec.a() == this->_widthVec.b()){
            vertex.insert(this->_lengthVec.a());
            vertex.insert(this->_lengthVec.b());
            vertex.insert(this->_widthVec.a());
        }
        else if (this->_lengthVec.b() == this->_widthVec.a()){
            vertex.insert(this->_lengthVec.a());
            vertex.insert(this->_lengthVec.b());
            vertex.insert(this->_widthVec.b());
        }
        else if (this->_lengthVec.b() == this->_widthVec.b()){
            vertex.insert(this->_lengthVec.a());
            vertex.insert(this->_lengthVec.b());
            vertex.insert(this->_widthVec.a());
        }
        return vertex;
    };

    void accept(ShapeVisitor* visitor) override {
        return visitor->visitRectangle(this);
    };
};