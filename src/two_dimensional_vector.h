#pragma once

#include "point.h"

#include <cmath>
#include <sstream>

class TwoDimensionalVector
{
private:
    Point _a;
    Point _b;

public:
    TwoDimensionalVector(const Point a, const Point b):_a(a), _b(b){}
    
    ~TwoDimensionalVector() {}

    Point a() const { return _a; }

    Point b() const { return _b; }

    double length() const {
        return sqrt(pow(_a.x()-_b.x(), 2) + pow(_a.y()-_b.y(), 2));
    }

    double dot(TwoDimensionalVector vec) const {
        return (this->a().x() - this->b().x()) * (vec.a().x() - vec.b().x()) + (this->a().y() - this->b().y()) * (vec.a().y() - vec.b().y());
    }

    double cross(TwoDimensionalVector vec) const {
        return (this->a().x() - this->b().x()) * (vec.a().y() - vec.b().y()) - (this->a().y() - this->b().y()) * (vec.a().x() - vec.b().x());
    }

    std::string info() const {
        std::stringstream ss;
        ss << std::fixed << std::setprecision(2) << "Vector (" << this->a().info() << ", " << this->b().info() << ")";
        std::string out = ss.str();
        return out;    
    }
};