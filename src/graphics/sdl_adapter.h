#include "canvas.h"
#include "./sdl/sdl.h"
#include <set>

class SDLAdapter : public Canvas 
{
private:
    SDL* _sdl;

public:
    SDLAdapter(int x, int y, SDL *sdl) : _sdl(sdl){
        if (x <= 0 || y <= 0)
            throw "error";
        else
            this->_sdl->init(x, y);
    }

    ~SDLAdapter() {}

    void drawCircle(Circle *cir) override {
        std::set<Point> points = cir->getPoints();
        std::set<Point>::iterator it = points.begin();
        double x = (*it).x() + cir->radius();
        double y = (*it).y() + cir->radius();

        this->_sdl->renderDrawCircle(x, y, cir->radius());
    }

    void drawTriangle(Triangle *tri) override {
        std::set<Point> points = tri->getPoints();
        double* line_point = new double[6];
        int index = 0;
        for (std::set<Point>::iterator it = points.begin(); it != points.end(); ++it){
            line_point[index] = (*it).x();
            line_point[index + 1] = (*it).y();
            index += 2;
        }
        this->_sdl->renderDrawLines(line_point, 6);
    }

    void drawRectangle(Rectangle *rect) override {
       std::set<Point> points = rect->getPoints();
        double* line_point = new double[8];
        int index = 0;
        for (std::set<Point>::iterator it = points.begin(); it != points.end(); ++it){
            line_point[index] = (*it).x();
            line_point[index + 1] = (*it).y();
            index += 2;
        }
        std::swap(line_point[4], line_point[6]);
        std::swap(line_point[5], line_point[7]);
        this->_sdl->renderDrawLines(line_point, 8);
    }

    void display() override {
        this->_sdl->renderPresent();
    }
};
