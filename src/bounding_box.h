#pragma once

#include <set>
#include <string>
#include "shape.h"
#include "point.h"

class BoundingBox
{
private:
    Point _max;
    Point _min;

public:
    BoundingBox(std::set<Point> points) : _max(calMaximumPoint(points)), _min(calMinimumPoint(points)) {}

    ~BoundingBox() {}

    Point calMaximumPoint(std::set<Point> &points) {
        if (points.size() == 0)
            throw "No Points"; 
        else{
            std::set<Point>::iterator it = points.begin();
            double max_x = (*it).x(), max_y = (*it).y();
            ++it;
            for (;it != points.end(); ++it){
                if ((*it).x() >= max_x)
                    max_x = (*it).x();
                if ((*it).y() >= max_y)
                    max_y = (*it).y();
            }
            return Point(max_x, max_y);
        }
    }

    Point calMinimumPoint(std::set<Point> &points) {
        if (points.size() == 0)
            throw "No Points";
        else{
            std::set<Point>::iterator it = points.begin();
            double min_x = (*it).x(), min_y = (*it).y();
            ++it;
            for (;it != points.end(); ++it){
                if ((*it).x() <= min_x)
                    min_x = (*it).x();
                if ((*it).y() <= min_y)
                    min_y = (*it).y();
            }
            return Point(min_x, min_y);
        }
    }

    const Point max() {
        return this->_max;
    }

    const Point min() {
        return this->_min;
    }

    bool collide(BoundingBox *box) {
        if (this->_max.x() < box->min().x() || this->_min.x() > box->max().x() || this->_max.y() < box->min().y() || this->_min.y() > box->max().y())
            return false;
        else
            return true;
    }
};