#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/circle.h"
#include "../src/visitor/collision_detector.h"

#include <set>
#include <algorithm>

TEST(CircleTest, RadiusTest){
    Point p1(0, 0);
    Point p2(0, 1);
    TwoDimensionalVector vec(p1, p2);
    Circle c(vec);
    ASSERT_NEAR(1, c.radius(), 0.001);    
}

TEST(CircleTest, AreaTest){
    Point p1(0, 0);
    Point p2(0, 1);
    TwoDimensionalVector vec(p1, p2);
    Circle c(vec);
    ASSERT_NEAR(3.1415, c.area(), 0.001);
}

TEST(CircleTest, PerimeterTest){
    Point p1(0, 0);
    Point p2(0, 1);
    TwoDimensionalVector vec(p1, p2);
    Circle c(vec);
    ASSERT_NEAR(6.2831, c.perimeter(), 0.001);
}

TEST(CircleTest, InfoTest){
    Point p1(-4.28, 0.26);
    Point p2(-4.83, 0.73);
    TwoDimensionalVector vec(p1, p2);
    Circle c(vec);
    ASSERT_EQ("Circle (Vector ((-4.28, 0.26), (-4.83, 0.73)))", c.info());
}

TEST(CircleTest, CreateDFSIteratorTest){
    Point p1(-4.28, 0.26);
    Point p2(-4.83, 0.73);
    TwoDimensionalVector vec(p1, p2);
    Circle c(vec);
    Iterator* it;
    ASSERT_NO_THROW(it = c.createIterator(IteratorFactory::getInstance("DFS")));
    delete it;
}

TEST(CircleTest, CreateBFSIteratorTest){
    Point p1(-4.28, 0.26);
    Point p2(-4.83, 0.73);
    TwoDimensionalVector vec(p1, p2);
    Circle c(vec);
    Iterator* it;
    ASSERT_NO_THROW(it = c.createIterator(IteratorFactory::getInstance("BFS")));
    delete it;
}

TEST(CircleTest, CreateListIteratorTest){
    Point p1(-4.28, 0.26);
    Point p2(-4.83, 0.73);
    TwoDimensionalVector vec(p1, p2);
    Circle c(vec);
    Iterator* it;
    ASSERT_NO_THROW(it = c.createIterator(IteratorFactory::getInstance("List")));
    delete it;
}

TEST(CircleTest, AddShapeTest){
    Point p1(-4.28, 0.26);
    Point p2(-4.83, 0.73);
    TwoDimensionalVector vec(p1, p2);
    Circle c1(vec);
    Shape* c2 = new Circle(vec);
    ASSERT_ANY_THROW(c1.addShape(c2));
    delete c2;
}

TEST(CircleTest, DeleteShapeTest){
    Point p1(-4.28, 0.26);
    Point p2(-4.83, 0.73);
    TwoDimensionalVector vec(p1, p2);
    Circle c1(vec);
    Shape* c2 = new Circle(vec);
    ASSERT_ANY_THROW(c1.deleteShape(c2));
    delete c2;;
}

TEST(CircleTest, GetPointsTest){
    Point p1(0, 0);
    Point p2(0, 2);
    TwoDimensionalVector vec(p1, p2);
    Circle c(vec);
    std::set<Point> circle_points = c.getPoints();
    Point p3(2, 2);
    Point p4(-2, -2);
    Point point_arr[] = {p3, p4};
    for (auto p : circle_points)
        ASSERT_TRUE(std::find(point_arr, point_arr+2, p) != point_arr+2);
}

TEST(CircleTest, CollisionDetectorTest){
    Point p1(0, 0);
    Point p2(0, 2);
    Point p3(0, 5);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p1, p3);
    Circle c1(vec1);
    Circle c2(vec2);
    CollisionDetector* visitor = new CollisionDetector(&c2);
    ASSERT_NO_THROW(c1.accept(visitor));
    delete visitor;
}