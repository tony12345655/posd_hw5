#include "../../src/builder/shape_builder.h"
#include "../../src/circle.h"
#include "../../src/rectangle.h"
#include "../../src/triangle.h"

#include <vector>

class ShapeBuilderTest : public ::testing::Test {
    protected:
        Point* p1 = new Point(0, 0);
        Point* p2 = new Point(3, 0);
        Point* p3 = new Point(3, 4);
        Point* p4 = new Point(0, 1);
        Point* p5 = new Point(2, 0);
        TwoDimensionalVector* vec1 = new TwoDimensionalVector(*p1, *p2);
        TwoDimensionalVector* vec2 = new TwoDimensionalVector(*p3, *p2);
        TwoDimensionalVector* vec3 = new TwoDimensionalVector(*p1, *p4);
        TwoDimensionalVector* vec4 = new TwoDimensionalVector(*p1, *p5);
        Triangle* tri;
        Rectangle* rec;
        Circle* cir;
        Triangle* tri1;
        Rectangle* rec1;
        Circle* cir1;
        CompoundShape* compound_shape;

        void SetUp() override {
            tri = new Triangle(*vec1, *vec3);
            rec = new Rectangle(*vec3, *vec4);
            cir = new Circle(*vec3);
            tri1 = new Triangle(*vec1, *vec3);
            rec1 = new Rectangle(*vec3, *vec4);
            cir1 = new Circle(*vec3);
            compound_shape = new CompoundShape();
            compound_shape->addShape(tri1);
            compound_shape->addShape(rec1);
            compound_shape->addShape(cir1);
        }

        void TearDown() override {
            delete p1;
            delete p2;
            delete p3;
            delete p4;
            delete p5;
            delete vec1;
            delete vec2;
            delete vec3;
            delete vec4;
            delete tri;
            delete rec;
            delete cir;
            delete compound_shape;
        }
};


TEST_F(ShapeBuilderTest, BuildCircle) {
    ShapeBuilder builder;
    builder.buildCircle(*p1, *p4);
    std::vector<Shape *> result = builder.getResult();
    ASSERT_EQ(cir->info(),  result[0]->info());
    for (auto shape : result)
        delete shape;
}

TEST_F(ShapeBuilderTest, BuildTriangle) {
    ShapeBuilder builder;
    builder.buildTriangle(*p1, *p2, *p4);
    std::vector<Shape *> result = builder.getResult();
    ASSERT_EQ(tri->info(), result[0]->info());
    for (auto shape : result)
        delete shape;
}

TEST_F(ShapeBuilderTest, BuildRectangle) {
    ShapeBuilder builder;
    builder.buildRectangle(*p1, *p4, *p5);
    std::vector<Shape *> result = builder.getResult();
    ASSERT_EQ(rec->info(),  result[0]->info());
    for (auto shape : result)
        delete shape;
}

TEST_F(ShapeBuilderTest, BuildCompoundShape) {
    ShapeBuilder builder;
    builder.buildCompoundShape();
    builder.buildTriangle(*p1, *p2, *p4);
    builder.buildRectangle(*p1, *p4, *p5);
    builder.buildCircle(*p1, *p4);
    builder.buildCompoundEnd();
    std::vector<Shape *> result = builder.getResult();
    ASSERT_EQ(compound_shape->info(),  result[0]->info());
    for (auto shape : result)
        delete shape;
}

TEST_F(ShapeBuilderTest, MultiLayerBuildCompoundShape) {
    ShapeBuilder builder;
    builder.buildCompoundShape();
    builder.buildCompoundShape();
    builder.buildRectangle(*p1, *p4, *p5);
    builder.buildCircle(*p1, *p4);
    builder.buildCompoundEnd();
    builder.buildCircle(*p1, *p4);
    builder.buildTriangle(*p2, *p1, *p4);
    builder.buildCompoundEnd();
    Rectangle* rec2 = new Rectangle(*vec3, *vec4);
    Triangle* tri2 = new Triangle(*vec1, *vec3);
    Circle* cir2 = new Circle(*vec3);
    Circle* cir3 = new Circle(*vec3);
    CompoundShape* cs1 = new CompoundShape();
    CompoundShape* cs2 = new CompoundShape();
    cs2->addShape(rec2);
    cs2->addShape(cir2);
    cs1->addShape(cs2);
    cs1->addShape(cir3);
    cs1->addShape(tri2);
    std::vector<Shape *> result = builder.getResult();
    Iterator* it = result[0]->createIterator(IteratorFactory::getInstance("List"));
    ASSERT_EQ(cs2->info(), it->currentItem()->info());
    delete it;
    delete cs1;
    for (auto shape : result)
        delete shape;
}

TEST_F(ShapeBuilderTest, GetResult){
    ShapeBuilder builder;
    builder.buildCircle(*p1, *p4);
    builder.buildTriangle(*p1, *p2, *p4);
    builder.buildCompoundShape();
    builder.buildTriangle(*p1, *p2, *p4);
    builder.buildRectangle(*p1, *p4, *p5);
    builder.buildCircle(*p1, *p4);
    builder.buildCompoundEnd();
    std::vector<Shape *> result = builder.getResult();
    ASSERT_EQ(cir->info(), result[0]->info());
    ASSERT_EQ(tri->info(), result[1]->info());
    ASSERT_EQ(compound_shape->info(), result[2]->info());
    for (auto shape : result)
        delete shape;
}
