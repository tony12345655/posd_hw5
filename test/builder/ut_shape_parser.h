#include "../../src/builder/shape_parser.h"
#include "../../src/circle.h"
#include <vector>

class ShapeParserTest : public ::testing::Test {
    protected:
        Point* p1 = new Point(0, 0);
        Point* p2 = new Point(3, 0);
        Point* p3 = new Point(3, 4);
        Point* p4 = new Point(0, 1);
        Point* p5 = new Point(2, 0);
        TwoDimensionalVector* vec1 = new TwoDimensionalVector(*p1, *p2);
        TwoDimensionalVector* vec2 = new TwoDimensionalVector(*p3, *p2);
        TwoDimensionalVector* vec3 = new TwoDimensionalVector(*p1, *p4);
        TwoDimensionalVector* vec4 = new TwoDimensionalVector(*p1, *p5);
        Triangle* tri;
        Rectangle* rec;
        Circle* cir;
        CompoundShape* compound_shape;

        void SetUp() override {
            tri = new Triangle(*vec1, *vec3);
            rec = new Rectangle(*vec3, *vec4);
            cir = new Circle(*vec3);
            compound_shape = new CompoundShape();
            compound_shape->addShape(tri);
            compound_shape->addShape(rec);
            compound_shape->addShape(cir);
        }

        void TearDown() override {
            delete p1;
            delete p2;
            delete p3;
            delete p4;
            delete p5;
            delete vec1;
            delete vec2;
            delete vec3;
            delete vec4;
            delete compound_shape;
        }
};

TEST_F(ShapeParserTest, CircleParse) {
    std::string input = "Circle (Vector ((0.00, 0.00), (0.00, 1.00)))";
    ShapeParser parser(input);
    parser.parse();
    std::vector<Shape*> result = parser.getResult();
    ASSERT_EQ(cir->info(), result[0]->info());
    for (auto shape : result)
        delete shape;
}

TEST_F(ShapeParserTest, TriangleParse) {
    std::string input = "Triangle (Vector ((0.00, 0.00), (3.00, 0.00)), Vector ((0.00, 0.00), (0.00, 1.00)))";
    ShapeParser parser(input);
    parser.parse();
    std::vector<Shape*> result = parser.getResult();
    ASSERT_EQ(tri->info(), result[0]->info());
    for (auto shape : result)
        delete shape;
}

TEST_F(ShapeParserTest, RectangleParse) {
    std::string input = "Rectangle (Vector ((0.00, 0.00), (0.00, 1.00)), Vector ((0.00, 0.00), (2.00, 0.00)))";
    ShapeParser parser(input);
    parser.parse();
    std::vector<Shape*> result = parser.getResult();
    ASSERT_EQ(rec->info(), result[0]->info());
    for (auto shape : result)
        delete shape;
}

TEST_F(ShapeParserTest, CompoundShapeParse) {
    std::string input = "CompoundShape (Triangle (Vector ((0.00, 0.00), (3.00, 0.00)), Vector ((0.00, 0.00), (0.00, 1.00))), Rectangle (Vector ((0.00, 0.00), (0.00, 1.00)), Vector ((0.00, 0.00), (2.00, 0.00))), Circle (Vector ((0.00, 0.00), (0.00, 1.00))))";
    ShapeParser parser(input);
    parser.parse();
    std::vector<Shape*> result = parser.getResult();
    ASSERT_EQ(compound_shape->info(), result[0]->info());
    for (auto shape : result)
        delete shape;
}