#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/shape.h"
#include "../src/triangle.h"
#include "../src/rectangle.h"
#include "../src/circle.h"
#include "../src/compound_shape.h"
#include "../src/visitor/collision_detector.h"

#include <set>
#include <algorithm>

class CompoundShapeTest : public ::testing::Test {
    protected:
        Point* p1 = new Point(0, 0);
        Point* p2 = new Point(3, 0);
        Point* p3 = new Point(3, 4);
        Point* p4 = new Point(0, 1);
        Point* p5 = new Point(2, 0);
        TwoDimensionalVector* vec1 = new TwoDimensionalVector(*p1, *p2);
        TwoDimensionalVector* vec2 = new TwoDimensionalVector(*p3, *p2);
        TwoDimensionalVector* vec3 = new TwoDimensionalVector(*p1, *p4);
        TwoDimensionalVector* vec4 = new TwoDimensionalVector(*p1, *p5);

        void TearDown() override {
            delete p1;
            delete p2;
            delete p3;
            delete p4;
            delete p5;
            delete vec1;
            delete vec2;
            delete vec3;
            delete vec4;
        }
};

TEST_F(CompoundShapeTest, LegalTest){
    Triangle* tri = new Triangle(*vec1, *vec2);
    Rectangle* rec = new Rectangle(*vec3, *vec4);
    Circle* cir = new Circle(*vec3);
    Shape* shape_arr[] = {tri, rec, cir};
    ASSERT_NO_THROW(CompoundShape compound_shape(shape_arr, 3));
}

TEST_F(CompoundShapeTest, AreaTest){
    Triangle* tri = new Triangle(*vec1, *vec2);
    Rectangle* rec = new Rectangle(*vec3, *vec4);
    Circle* cir = new Circle(*vec3);
    Shape* shape_arr[] = {tri, rec, cir};
    CompoundShape compound_shape(shape_arr, 3);
    ASSERT_NEAR(8 + M_PI, compound_shape.area(), 0.001);
}

TEST_F(CompoundShapeTest, PerimeterTest){
    Triangle* tri = new Triangle(*vec1, *vec2);
    Rectangle* rec = new Rectangle(*vec3, *vec4);
    Circle* cir = new Circle(*vec3);
    Shape* shape_arr[] = {tri, rec, cir};
    CompoundShape compound_shape(shape_arr, 3);
    ASSERT_NEAR(18 + 2*M_PI, compound_shape.perimeter(), 0.001);
}

TEST_F(CompoundShapeTest, InfoTest){
    Rectangle* rec = new Rectangle(*vec3, *vec4);
    Circle* cir = new Circle(*vec3);
    Shape* shape_arr[] = {cir, rec};
    CompoundShape compound_shape(shape_arr, 2);
    ASSERT_EQ("CompoundShape (Circle (Vector ((0.00, 0.00), (0.00, 1.00))), Rectangle (Vector ((0.00, 0.00), (0.00, 1.00)), Vector ((0.00, 0.00), (2.00, 0.00))))", compound_shape.info());
}

TEST_F(CompoundShapeTest, InfoNoChildTest){
    CompoundShape* compound_shape = new CompoundShape();
    ASSERT_EQ("CompoundShape ()", compound_shape->info());
    delete compound_shape;
}

TEST_F(CompoundShapeTest, TwoCompoundShapeInfoTest){
    Rectangle* rec = new Rectangle(*vec3, *vec4);
    Circle* cir = new Circle(*vec3);
    Shape* shape_arr1[] = {cir};
    Shape* shape_arr2[] = {rec};
    CompoundShape* compound_shape1 = new CompoundShape(shape_arr1, 1);
    CompoundShape* compound_shape2 = new CompoundShape(shape_arr2, 1);
    Shape* shape_arr3[] = {compound_shape1, compound_shape2};
    CompoundShape compound_shape3(shape_arr3, 2);
    ASSERT_EQ("CompoundShape (CompoundShape (Circle (Vector ((0.00, 0.00), (0.00, 1.00)))), CompoundShape (Rectangle (Vector ((0.00, 0.00), (0.00, 1.00)), Vector ((0.00, 0.00), (2.00, 0.00)))))", compound_shape3.info());
}

TEST_F(CompoundShapeTest, CreateDFSIteratorTest){
    Triangle* tri = new Triangle(*vec1, *vec2);
    Rectangle* rec = new Rectangle(*vec3, *vec4);
    Circle* cir = new Circle(*vec3);
    Shape* shape_arr[] = {tri, rec, cir};
    CompoundShape compound_shape(shape_arr, 3);
    Iterator* it;
    ASSERT_NO_THROW(it = compound_shape.createIterator(IteratorFactory::getInstance("DFS")));
    delete it;
}

TEST_F(CompoundShapeTest, CreateBFSIteratorTest){
    Triangle* tri = new Triangle(*vec1, *vec2);
    Rectangle* rec = new Rectangle(*vec3, *vec4);
    Circle* cir = new Circle(*vec3);
    Shape* shape_arr[] = {tri, rec, cir};
    CompoundShape compound_shape(shape_arr, 3);
    Iterator* it;
    ASSERT_NO_THROW(it = compound_shape.createIterator(IteratorFactory::getInstance("BFS")));
    delete it;
}

TEST_F(CompoundShapeTest, CreateListIteratorTest){
    Triangle* tri = new Triangle(*vec1, *vec2);
    Rectangle* rec = new Rectangle(*vec3, *vec4);
    Circle* cir = new Circle(*vec3);
    Shape* shape_arr[] = {tri, rec, cir};
    CompoundShape compound_shape(shape_arr, 3);
    Iterator* it;
    ASSERT_NO_THROW(it = compound_shape.createIterator(IteratorFactory::getInstance("List")));
    delete it;
}

TEST_F(CompoundShapeTest, AddShape){
    Point p1(0, 0);
    Point p2(0, 1);
    TwoDimensionalVector vec(p1, p2);
    Shape* new_shape = new Circle(vec);
    CompoundShape* compound_shape = new CompoundShape();
    compound_shape->addShape(new_shape);
    ASSERT_NEAR(M_PI, compound_shape->area(), 0.001);
    delete compound_shape;
}

TEST_F(CompoundShapeTest, DeleteShape){
    Triangle* tri = new Triangle(*vec1, *vec2);
    Rectangle* rec = new Rectangle(*vec3, *vec4);
    Circle* cir = new Circle(*vec3);
    Shape* shape_arr[] = {tri, rec};
    CompoundShape* compound_shape1 = new CompoundShape(shape_arr, 2);
    CompoundShape* compound_shape2 = new CompoundShape();
    compound_shape2->addShape(cir);
    compound_shape2->addShape(compound_shape1);
    compound_shape2->deleteShape(tri);
    ASSERT_NEAR(2 + M_PI, compound_shape2->area(), 0.001);
    delete compound_shape2;
}

TEST_F(CompoundShapeTest, GetPointsTest){
    Triangle* tri = new Triangle(*vec1, *vec2);
    Circle* cir = new Circle(*vec3);
    Point p1(0, 0);
    Point p2(0 ,1);
    TwoDimensionalVector vec(p1, p2);
    Circle* cir1 = new Circle(vec);
    Circle* cir2 = new Circle(vec);
    Shape* shape_arr[] = {cir, tri};
    CompoundShape* compound_shape1 = new CompoundShape(shape_arr, 2);
    CompoundShape* compound_shape2 = new CompoundShape();
    compound_shape2->addShape(cir1);
    compound_shape2->addShape(cir2);
    compound_shape1->addShape(compound_shape2);
    std::set<Point> compound_shape_points = compound_shape1->getPoints();
    Point p3(-1, -1);
    Point p4(1, 1);
    Point p5(0, 0);
    Point p6(3, 0);
    Point p7(3, 4);
    Point point_arr[] = {p3, p4, p5, p6, p7};
    for (auto p : compound_shape_points)
        ASSERT_TRUE(std::find(point_arr, point_arr+5, p) != point_arr+5);
    delete compound_shape1;
}

TEST_F(CompoundShapeTest, CollisionDetectorTest){
    Rectangle* rec = new Rectangle(*vec3, *vec4);
    Circle* cir = new Circle(*vec3);
    Point p1(0, 0);
    Point p2(0 ,2);
    TwoDimensionalVector vec(p1, p2);
    Circle* c = new Circle(vec);
    CompoundShape* compound_shape = new CompoundShape();
    compound_shape->addShape(cir);
    compound_shape->addShape(rec);
    CollisionDetector* visitor = new CollisionDetector(c);
    ASSERT_NO_THROW(compound_shape->accept(visitor));
    delete c;
    delete compound_shape;
    delete visitor;
}