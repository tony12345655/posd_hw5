#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/circle.h"
#include "../src/rectangle.h"
#include "../src/triangle.h"
#include "../src/compound_shape.h"
#include "../src/bounding_box.h"

class BoundingTest : public ::testing::Test {
    protected:
        Point* p1;
        Point* p2;
        Point* p3;
        Point* p4;
        Point* p5;
        TwoDimensionalVector* vec1;
        TwoDimensionalVector* vec2;
        TwoDimensionalVector* vec3;
        TwoDimensionalVector* vec4;

        void SetUp() override {
            p1 = new Point(0, 0);
            p2 = new Point(3, 0);
            p3 = new Point(3, 4);
            p4 = new Point(0, 1);
            p5 = new Point(2, 0);
            vec1 = new TwoDimensionalVector(*p1, *p2);
            vec2 = new TwoDimensionalVector(*p3, *p2);
            vec3 = new TwoDimensionalVector(*p1, *p4);
            vec4 = new TwoDimensionalVector(*p1, *p5);
        }

        void TearDown() override {
            delete p1;
            delete p2;
            delete p3;
            delete p4;
            delete p5;
            delete vec1;
            delete vec2;
            delete vec3;
            delete vec4;
        }
};

TEST_F(BoundingTest, CircleLegalTest){
    Circle cir(*vec3);
    BoundingBox* bounding;
    ASSERT_NO_THROW(bounding = new BoundingBox (cir.getPoints()));
    delete bounding;
}

TEST_F(BoundingTest, TriangleLegalTest){
    Triangle tri(*vec1, *vec2);
    BoundingBox* bounding;
    ASSERT_NO_THROW(bounding = new BoundingBox (tri.getPoints()));
    delete bounding;
}

TEST_F(BoundingTest, RectangleLegalTest){
    Rectangle rec(*vec3, *vec4);
    BoundingBox* bounding;
    ASSERT_NO_THROW(bounding = new BoundingBox (rec.getPoints()));
    delete bounding;
}

TEST_F(BoundingTest, CompoundShapeLegalTest){
    Triangle* tri = new Triangle(*vec1, *vec2);
    Rectangle* rec = new Rectangle(*vec3, *vec4);
    CompoundShape* compound_shape = new CompoundShape();
    compound_shape->addShape(tri);
    compound_shape->addShape(rec);
    BoundingBox* bounding;
    ASSERT_NO_THROW(bounding = new BoundingBox (compound_shape->getPoints()));
    delete compound_shape;
    delete bounding;
}

TEST_F(BoundingTest, IllegalTest){
    CompoundShape* compound_shape = new CompoundShape();
    BoundingBox* bounding;
    ASSERT_ANY_THROW(bounding = new BoundingBox (compound_shape->getPoints()));
    delete compound_shape;
}

TEST_F(BoundingTest, CalMaximumPointTest){
    Triangle tri(*vec1, *vec2);
    Rectangle rec(*vec3, *vec4);
    BoundingBox rec_bound(rec.getPoints());
    std::set<Point> points = tri.getPoints();
    Point p1 = rec_bound.calMaximumPoint(points);
    Point p2 = rec_bound.calMaximumPoint(points);
    ASSERT_EQ(3, p1.x());
    ASSERT_EQ(4, p2.y());
}

TEST_F(BoundingTest, CalMinimumPointTest){
    Triangle tri(*vec1, *vec2);
    Rectangle rec(*vec3, *vec4);
    BoundingBox rec_bound(rec.getPoints());
    std::set<Point> points = tri.getPoints();
    Point p1 = rec_bound.calMinimumPoint(points);
    Point p2 = rec_bound.calMinimumPoint(points);
    ASSERT_EQ(0, p1.x());
    ASSERT_EQ(0, p2.y());
}

TEST_F(BoundingTest, MaxTest){
    Rectangle rec (*vec3, *vec4);
    BoundingBox rec_bound(rec.getPoints());
    ASSERT_EQ(2, rec_bound.max().x());
    ASSERT_EQ(1, rec_bound.max().y());
}

TEST_F(BoundingTest, MinTest){
    Rectangle rec (*vec3, *vec4);
    BoundingBox rec_bound(rec.getPoints());
    ASSERT_EQ(0, rec_bound.min().x());
    ASSERT_EQ(0, rec_bound.min().y());
}

TEST_F(BoundingTest, CollideTest){
    Triangle tri(*vec1, *vec2);
    Rectangle rec(*vec3, *vec4);
    BoundingBox* rec_bound = new BoundingBox(rec.getPoints());
    BoundingBox* tri_bound = new BoundingBox(tri.getPoints());
    ASSERT_TRUE(rec_bound->collide(tri_bound));
    delete rec_bound;
    delete tri_bound;    
}

TEST_F(BoundingTest, NoCollideTest){
    Rectangle rec(*vec3, *vec4);
    Point p1(10, 10);
    Point p2(10, 15);
    TwoDimensionalVector vec(p1, p2);
    Circle c(vec);
    BoundingBox* rec_bound = new BoundingBox(rec.getPoints());
    BoundingBox* cir_bound = new BoundingBox(c.getPoints());
    ASSERT_FALSE(rec_bound->collide(cir_bound));
    delete rec_bound;
    delete cir_bound;    
}

TEST_F(BoundingTest, CompoundCollideTest){
    Triangle* tri = new Triangle(*vec1, *vec2);
    Rectangle* rec = new Rectangle(*vec3, *vec4);
    CompoundShape* compound_shape = new CompoundShape();
    compound_shape->addShape(tri);
    compound_shape->addShape(rec);
    BoundingBox* compound_shape_bound = new BoundingBox(compound_shape->getPoints());
    BoundingBox* tri_bound = new BoundingBox(tri->getPoints());
    ASSERT_TRUE(compound_shape_bound->collide(tri_bound));
    delete compound_shape;
    delete compound_shape_bound;
    delete tri_bound;
}

TEST_F(BoundingTest, NoCompoundCollideTest){
    Triangle* tri = new Triangle(*vec1, *vec2);
    Rectangle* rec = new Rectangle(*vec3, *vec4);
    Point p1(10, 10);
    Point p2(10, 15);
    TwoDimensionalVector vec(p1, p2);
    Circle c(vec);
    CompoundShape* compound_shape = new CompoundShape();
    compound_shape->addShape(tri);
    compound_shape->addShape(rec);
    BoundingBox* compound_shape_bound = new BoundingBox(compound_shape->getPoints());
    BoundingBox* cir_bound = new BoundingBox(c.getPoints());
    ASSERT_FALSE(compound_shape_bound->collide(cir_bound));
    delete compound_shape;
    delete compound_shape_bound;
    delete cir_bound;
}