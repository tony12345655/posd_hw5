#include "../../src/graphics/sdl_adapter.h"
#include "mock_sdl_renderer.h"
#include "../../src/circle.h"
#include "../../src/rectangle.h"
#include "../../src/triangle.h"

TEST(SDLAdapterTest, InitShouldBeCalled) {
    MockSDLRenderer* mockSDLRenderer = new MockSDLRenderer();
    Canvas *canvas = new SDLAdapter(1024, 768, mockSDLRenderer);
    ASSERT_TRUE(mockSDLRenderer->isInitCalled());
    ASSERT_EQ(1024, mockSDLRenderer->initWidth());
    ASSERT_EQ(768, mockSDLRenderer->initHeight());
    delete mockSDLRenderer;
    delete canvas;
}

TEST(SDLAdapterTest, InitShouldBeAnyThrow) {
    MockSDLRenderer* mockSDLRenderer = new MockSDLRenderer();
    Canvas *canvas;
    ASSERT_ANY_THROW(new SDLAdapter(0, 0, mockSDLRenderer));
    ASSERT_ANY_THROW(new SDLAdapter(1024, 0, mockSDLRenderer));
    ASSERT_ANY_THROW(new SDLAdapter(0, 768, mockSDLRenderer));
    ASSERT_FALSE(mockSDLRenderer->isInitCalled());
    delete mockSDLRenderer;
}

TEST(SDLAdapterTest, DrawCircle) {
    MockSDLRenderer* mockSDLRenderer = new MockSDLRenderer();
    Canvas *canvas = new SDLAdapter(1024, 768, mockSDLRenderer);
    Point p1(0, 0);
    Point p2(0, 1);
    TwoDimensionalVector v(p1, p2);
    Circle cir(v);
    canvas->drawCircle(&cir);
    const double* circleArg = mockSDLRenderer->renderDrawCircleCalledArgs();
    ASSERT_NEAR(0, circleArg[0], 0.001);
    ASSERT_NEAR(0, circleArg[1], 0.001);
    ASSERT_NEAR(1, circleArg[2], 0.001);
    delete mockSDLRenderer;
    delete canvas;
}

TEST(SDLAdapterTest, DrawTriangle) {
    MockSDLRenderer* mockSDLRenderer = new MockSDLRenderer();
    Canvas *canvas = new SDLAdapter(1024, 768, mockSDLRenderer);
    Point p1(0, 0);
    Point p2(3, 0);
    Point p3(3, 4);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p3, p2);
    Triangle tri(vec1, vec2);
    canvas->drawTriangle(&tri);
    const double* points = mockSDLRenderer->renderDrawLinesCalledPoints();
    double size = mockSDLRenderer->renderDrawLinesCalledSize();
    ASSERT_NEAR(0, points[0], 0.001);
    ASSERT_NEAR(0, points[1], 0.001);
    ASSERT_NEAR(3, points[2], 0.001);
    ASSERT_NEAR(0, points[3], 0.001);
    ASSERT_NEAR(3, points[4], 0.001);
    ASSERT_NEAR(4, points[5], 0.001);
    ASSERT_NEAR(6, size, 0.001);
    delete mockSDLRenderer;
    delete canvas;
    delete[] points;
}

TEST(SDLAdapterTest, DrawRectangle) {
    MockSDLRenderer* mockSDLRenderer = new MockSDLRenderer();
    Canvas *canvas = new SDLAdapter(1024, 768, mockSDLRenderer);
    Point p1(0, 0);
    Point p2(0, 1);
    Point p3(1, 0);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p1, p3);
    Rectangle rec(vec1, vec2);
    canvas->drawRectangle(&rec);
    const double* points = mockSDLRenderer->renderDrawLinesCalledPoints();
    double size = mockSDLRenderer->renderDrawLinesCalledSize();
    ASSERT_NEAR(0, points[0], 0.001);
    ASSERT_NEAR(0, points[1], 0.001);
    ASSERT_NEAR(0, points[2], 0.001);
    ASSERT_NEAR(1, points[3], 0.001);
    ASSERT_NEAR(1, points[4], 0.001);
    ASSERT_NEAR(1, points[5], 0.001);
    ASSERT_NEAR(1, points[6], 0.001);
    ASSERT_NEAR(0, points[7], 0.001);
    ASSERT_NEAR(8, size, 0.001);
    delete mockSDLRenderer;
    delete canvas;
    delete[] points;
}

TEST(SDLAdapterTest, DisplayFalse) {
    MockSDLRenderer* mockSDLRenderer = new MockSDLRenderer();
    Canvas *canvas = new SDLAdapter(1024, 768, mockSDLRenderer);
    ASSERT_FALSE(mockSDLRenderer->isRenderPresentCalled());
    delete mockSDLRenderer;
    delete canvas;
}

TEST(SDLAdapterTest, DisplayTrue) {
    MockSDLRenderer* mockSDLRenderer = new MockSDLRenderer();
    Canvas *canvas = new SDLAdapter(1024, 768, mockSDLRenderer);
    canvas->display();
    ASSERT_TRUE(mockSDLRenderer->isRenderPresentCalled());
    delete mockSDLRenderer;
    delete canvas;
}