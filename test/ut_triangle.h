#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/triangle.h"
#include "../src/visitor/collision_detector.h"

#include <set>
#include <algorithm>

TEST(TriangleTst, LegalTest){
    Point p1(3, 0);
    Point p2(1, 0);
    Point p3(7, 1);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p1, p3);
    TwoDimensionalVector vec3(p3, p1);
    TwoDimensionalVector vec4(p2, p1);
    Triangle* tri1;
    Triangle* tri2;
    Triangle* tri3;
    Triangle* tri4;
    // 向量A的x與向量B的x相同
    ASSERT_NO_THROW(tri1 = new Triangle(vec1, vec2));
    // 向量A的x與向量B的y相同
    ASSERT_NO_THROW(tri2 = new Triangle(vec1, vec3));
    // 向量A的y與向量B的x相同
    ASSERT_NO_THROW(tri3 = new Triangle(vec4, vec2));
    // 向量A的y與向量B的y相同
    ASSERT_NO_THROW(tri4 = new Triangle(vec4, vec3));
    delete tri1;
    delete tri2;
    delete tri3;
    delete tri4;

}

TEST(TriangleTest, IllegalTest){
    Point p1(0, 0);
    Point p2(1, 0);
    Point p3(3, 4);
    Point p4(3, 0);
    Point p5(2, 2);
    Point p6(0, 2);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p3, p4);
    TwoDimensionalVector vec3(p3, p2);
    TwoDimensionalVector vec4(p5, p6);
    Triangle* tri1;
    Triangle* tri2;
    Triangle* tri3;
    Triangle* tri4;
    // 兩向量無任一點重疊
    ASSERT_ANY_THROW(tri1 = new Triangle(vec1, vec2));
    // 兩向量為同一向量
    ASSERT_ANY_THROW(tri2 = new Triangle(vec3, vec3));
    // 兩向量平行
    ASSERT_ANY_THROW(tri3 = new Triangle(vec1, vec4));
}

TEST(TriangleTest, AreaTest){
    Point p1(0, 0);
    Point p2(3, 0);
    Point p3(3, 4);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p3, p2);
    Triangle tri(vec1, vec2);
    ASSERT_NEAR(6, tri.area(), 0.001);
}

TEST(TriangleTest, PerimeterTest){
    Point p1(0, 0);
    Point p2(3, 0);
    Point p3(3, 4);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p3, p2);
    Triangle tri(vec1, vec2);
    ASSERT_NEAR(12, tri.perimeter(), 0.001);
}

TEST(TriangleTest, InfoTest){
    Point p1(0, 0);
    Point p2(3, 0);
    Point p3(3, 4);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p3, p2);
    Triangle tri(vec1, vec2);
    ASSERT_EQ("Triangle (Vector ((0.00, 0.00), (3.00, 0.00)), Vector ((3.00, 4.00), (3.00, 0.00)))", tri.info());
}

TEST(TriangleTest, CreateDFSIteratorTest){
    Point p1(0, 0);
    Point p2(3, 0);
    Point p3(3, 4);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p3, p2);
    Triangle tri(vec1, vec2);
    Iterator* it;
    ASSERT_NO_THROW(it = tri.createIterator(IteratorFactory::getInstance("DFS")));
    delete it;
}

TEST(TriangleTest, CreateBFSIteratorTest){
    Point p1(0, 0);
    Point p2(3, 0);
    Point p3(3, 4);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p3, p2);
    Triangle tri(vec1, vec2);
    Iterator* it;
    ASSERT_NO_THROW(it = tri.createIterator(IteratorFactory::getInstance("BFS")));
    delete it;
}

TEST(TriangleTest, CreateListIteratorTest){
    Point p1(0, 0);
    Point p2(3, 0);
    Point p3(3, 4);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p3, p2);
    Triangle tri(vec1, vec2);
    Iterator* it;
    ASSERT_NO_THROW(it = tri.createIterator(IteratorFactory::getInstance("List")));
    delete it;
}

TEST(TriangleTest, AddShapeTest){
    Point p1(0, 0);
    Point p2(3, 0);
    Point p3(3, 4);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p3, p2);
    Triangle tri1(vec1, vec2);
    Shape* tri2 = new Triangle(vec1, vec2);
    ASSERT_ANY_THROW(tri1.addShape(tri2));
    delete tri2;
}

TEST(TriangleTest, DeleteShapeTest){
    Point p1(0, 0);
    Point p2(3, 0);
    Point p3(3, 4);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p3, p2);
    Triangle tri1(vec1, vec2);
    Shape* tri2 = new Triangle(vec1, vec2);
    ASSERT_ANY_THROW(tri1.deleteShape(tri2));
    delete tri2;
}

TEST(TriangleTest, GetPointsTest){
    Point p1(0, 0);
    Point p2(3, 0);
    Point p3(3, 4);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p3, p2);
    Triangle tri(vec1, vec2);
    std::set<Point> triangle_points = tri.getPoints();
    Point point_arr[] = {p1, p2, p3};
    for (auto p : triangle_points)
        ASSERT_TRUE(std::find(point_arr, point_arr+3, p) != point_arr+3);
}

TEST(TriangleTest, CollisionDetectorTest){
    Point p1(0, 0);
    Point p2(3, 0);
    Point p3(3, 4);
    TwoDimensionalVector vec1(p1, p2);
    TwoDimensionalVector vec2(p3, p2);
    Triangle tri1(vec1, vec2);
    Triangle tri2(vec1, vec2);
    CollisionDetector* visitor = new CollisionDetector(&tri2);
    ASSERT_NO_THROW(tri1.accept(visitor));
    delete visitor;
}