#include "../../src/compound_shape.h"
#include "../../src/shape.h"
#include "../../src/rectangle.h"

class DFSCompoundIteratorTest : public ::testing::Test
{
protected:
    Point *p1, *p2, *p3, *p4;
    TwoDimensionalVector *vec1, *vec2, *vec3;
    Circle *cir1, *cir2;
    Rectangle* rec;
    CompoundShape *cs1, *cs2;
    Iterator* it;

    void SetUp() override
    {
        p1 = new Point(0, 0);
        p2 = new Point(0, 5);
        p3 = new Point(5, 0);
        p4 = new Point(0, 3);

        vec1 = new TwoDimensionalVector(*p1, *p2);
        vec2 = new TwoDimensionalVector(*p1, *p3);
        vec3 = new TwoDimensionalVector(*p1, *p4);

        cs1 = new CompoundShape();
        cir1 = new Circle(*vec1);
        rec = new Rectangle(*vec1, *vec2);
        cs1->addShape(cir1);
        cs1->addShape(rec);

        cs2 = new CompoundShape();
        cir2 = new Circle(*vec3);
        cs2->addShape(cir2);
        cs2->addShape(cs1);

        it = cs2->createIterator(IteratorFactory::getInstance("DFS"));
    }

    void TearDown() override
    {
        delete p1;
        delete p2;
        delete p3;
        delete p4;
        delete vec1;
        delete vec2;
        delete vec3;
        delete cs2;
        delete it;
    }
};

TEST_F(DFSCompoundIteratorTest, CurrentItemShouldBeCorrect)
{
    ASSERT_EQ(3 * 3 * M_PI, it->currentItem()->area());
}

TEST_F(DFSCompoundIteratorTest, NextShouldBeCorrect)
{
    it->next();
    ASSERT_EQ(5 * 5 * M_PI + 25, it->currentItem()->area());
    it->next();
    ASSERT_EQ(5 * 5 * M_PI, it->currentItem()->area());
}


TEST_F(DFSCompoundIteratorTest, IsDoneShouldBeCorrect)
{
    it->next();
    it->next();
    it->next();
    it->next();

    ASSERT_TRUE(it->isDone());
}

TEST_F(DFSCompoundIteratorTest, CurrentItemShouldThrowExceptionWhenIsDone)
{
    it->next();
    it->next();
    it->next();
    it->next();

    ASSERT_ANY_THROW(it->next());
}

TEST_F(DFSCompoundIteratorTest, NextShouldThrowExceptionWhenIsDone)
{
    it->next();
    it->next();
    it->next();
    it->next();
    
    ASSERT_ANY_THROW(it->currentItem());
}

TEST_F(DFSCompoundIteratorTest, OrderShouldBeCorrectIfNoChildrenInCompound){
    CompoundShape* compound_shape = new CompoundShape();
    Iterator* it = compound_shape->createIterator(IteratorFactory::getInstance("DFS"));
    ASSERT_TRUE(it->isDone());
    delete compound_shape;
    delete it;
}

TEST_F(DFSCompoundIteratorTest, ManyCompoundTest){
    Shape *cir1 = new Circle(*vec1);
    Shape *cir2 = new Circle(*vec2);
    Shape *cir3 = new Circle(*vec3);
    TwoDimensionalVector vec4(*p2, *p3);
    Shape *cir4 = new Circle(vec4);
    Shape *sps1[] = {cir1};
    Shape *cs1 = new CompoundShape(sps1, 1);
    Shape *sps2[] = {cs1, cir2};
    Shape *cs2 = new CompoundShape(sps2, 2);
    Shape *sps3[] = {cir3, cs2};
    Shape *cs3 = new CompoundShape(sps3, 2);
    Shape *sps4[] = {cs3, cir4};
    Shape *cs4 = new CompoundShape(sps4, 2);
    Iterator *it = cs4->createIterator(IteratorFactory::getInstance("DFS"));
    ASSERT_FALSE(it->isDone());
    ASSERT_EQ(it->currentItem(), cs3);
    ASSERT_EQ(it->currentItem(), cs3);
    ASSERT_NO_THROW(it->next());

    ASSERT_FALSE(it->isDone());
    ASSERT_EQ(it->currentItem(), cir3);
    ASSERT_EQ(it->currentItem(), cir3);
    ASSERT_NO_THROW(it->next());

    ASSERT_FALSE(it->isDone());
    ASSERT_EQ(it->currentItem(), cs2);
    ASSERT_EQ(it->currentItem(), cs2);
    ASSERT_NO_THROW(it->next());

    ASSERT_FALSE(it->isDone());
    ASSERT_EQ(it->currentItem(), cs1);
    ASSERT_EQ(it->currentItem(), cs1);
    ASSERT_NO_THROW(it->next());

    ASSERT_FALSE(it->isDone());
    ASSERT_EQ(it->currentItem(), cir1);
    ASSERT_EQ(it->currentItem(), cir1);
    ASSERT_NO_THROW(it->next());

    ASSERT_FALSE(it->isDone());
    ASSERT_EQ(it->currentItem(), cir2);
    ASSERT_EQ(it->currentItem(), cir2);
    ASSERT_NO_THROW(it->next());

    ASSERT_FALSE(it->isDone());
    ASSERT_EQ(it->currentItem(), cir4);
    ASSERT_EQ(it->currentItem(), cir4);
    ASSERT_NO_THROW(it->next());

    ASSERT_TRUE(it->isDone());
    ASSERT_ANY_THROW(it->next());
    ASSERT_ANY_THROW(it->currentItem());

    delete it;
    delete cs4;

}
