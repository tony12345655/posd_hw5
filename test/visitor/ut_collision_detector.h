#include "../../src/shape.h"
#include "../../src/circle.h"
#include "../../src/triangle.h"
#include "../../src/rectangle.h"
#include "../../src/compound_shape.h"
#include "../../src/bounding_box.h"
#include "../../src/visitor/shape_visitor.h"
#include "../../src/visitor/collision_detector.h"

#include <vector>

class CollisionDetectorTest : public ::testing::Test {
    protected:
        Point* p1;
        Point* p2;
        Point* p3;
        Point* p4;
        Point* p5;
        TwoDimensionalVector* vec1;
        TwoDimensionalVector* vec2;
        TwoDimensionalVector* vec3;
        TwoDimensionalVector* vec4;
        Triangle* tri1;
        Rectangle* rec1;
        Circle* cir1;
        Triangle* tri;
        Rectangle* rec;
        Circle* cir;
        CompoundShape* compound_shape;

        void SetUp() override {
            p1 = new Point(0, 0);
            p2 = new Point(3, 0);
            p3 = new Point(3, 4);
            p4 = new Point(0, 1);
            p5 = new Point(2, 0);
            vec1 = new TwoDimensionalVector(*p1, *p2);
            vec2 = new TwoDimensionalVector(*p3, *p2);
            vec3 = new TwoDimensionalVector(*p1, *p4);
            vec4 = new TwoDimensionalVector(*p1, *p5);
            tri = new Triangle(*vec1, *vec2);
            rec = new Rectangle(*vec3, *vec4);
            cir = new Circle(*vec3);
            tri1 = new Triangle(*vec1, *vec2);
            rec1 = new Rectangle(*vec3, *vec4);
            cir1 = new Circle(*vec3);
            compound_shape = new CompoundShape();
            compound_shape->addShape(tri1);
            compound_shape->addShape(rec1);
            compound_shape->addShape(cir1);
        }

        void TearDown() override {
            delete p1;
            delete p2;
            delete p3;
            delete p4;
            delete p5;
            delete vec1;
            delete vec2;
            delete vec3;
            delete vec4;
            delete tri;
            delete rec;
            delete cir;
            delete compound_shape;
        }
};

TEST_F(CollisionDetectorTest, LegalTest){
    CollisionDetector* cd;
    ASSERT_NO_THROW(cd = new CollisionDetector(cir));
    delete cd;
}

TEST_F(CollisionDetectorTest, VisitCircleCollidedTest){
    CollisionDetector* visitor = new CollisionDetector(compound_shape);
    cir->accept(visitor);
    std::vector<Shape *> scores = visitor->collidedShapes();
    ASSERT_EQ(cir, (*scores.begin()));
    delete visitor;
}

TEST_F(CollisionDetectorTest, VisitCircleNoCollidedTest){
    Point p1(10, 10);
    Point p2(10, 15);
    TwoDimensionalVector vec(p1, p2);
    Circle* c = new Circle(vec);
    CollisionDetector* visitor = new CollisionDetector(compound_shape);
    c->accept(visitor);
    std::vector<Shape *> scores = visitor->collidedShapes();
    ASSERT_EQ(0, scores.size());
    delete c;
    delete visitor;
}

TEST_F(CollisionDetectorTest, VisitTriangleCollidedTest){
    CollisionDetector* visitor = new CollisionDetector(compound_shape);
    tri->accept(visitor);
    std::vector<Shape *> scores = visitor->collidedShapes();
    ASSERT_EQ(tri, (*scores.begin()));
    delete visitor;
}

TEST_F(CollisionDetectorTest, VisitTriangleNoCollidedTest){
    Point p1(10, 10);
    Point p2(10, 15);
    Point p3(15, 10);
    TwoDimensionalVector v1(p1, p2);
    TwoDimensionalVector v2(p1, p3);
    Triangle* t = new Triangle(v1, v2);
    CollisionDetector* visitor = new CollisionDetector(compound_shape);
    t->accept(visitor);
    std::vector<Shape *> scores = visitor->collidedShapes();
    ASSERT_EQ(0, scores.size());
    delete t;
    delete visitor;
}

TEST_F(CollisionDetectorTest, VisitRectangleTest){
    CollisionDetector* visitor = new CollisionDetector(compound_shape);
    rec->accept(visitor);
    std::vector<Shape *> scores = visitor->collidedShapes();
    ASSERT_EQ(rec, (*scores.begin()));
    delete visitor;
}

TEST_F(CollisionDetectorTest, VisitNoRectangleTest){
    Point p1(10, 10);
    Point p2(10, 15);
    Point p3(15, 10);
    TwoDimensionalVector v1(p1, p2);
    TwoDimensionalVector v2(p1, p3);
    Rectangle* r = new Rectangle(v1, v2);
    CollisionDetector* visitor = new CollisionDetector(compound_shape);
    r->accept(visitor);
    std::vector<Shape *> scores = visitor->collidedShapes();
    ASSERT_EQ(0, scores.size());
    delete r;
    delete visitor;
}

TEST_F(CollisionDetectorTest, VisitCompoundShapeOne){
    CollisionDetector* visitor = new CollisionDetector(cir);
    compound_shape->accept(visitor);
    std::vector<Shape *> scores = visitor->collidedShapes();
    ASSERT_EQ(tri1, (*scores.begin()));
    delete visitor;
}

TEST_F(CollisionDetectorTest, VisitCompoundShapeTwo){
    Point p1(10, 10);
    Point p2(10, 15);
    TwoDimensionalVector vec(p1, p2);
    Circle* c = new Circle(vec);
    CollisionDetector* visitor = new CollisionDetector(c);
    compound_shape->accept(visitor);
    std::vector<Shape *> scores = visitor->collidedShapes();
    ASSERT_EQ(0, scores.size());
    delete c;
    delete visitor;
}
